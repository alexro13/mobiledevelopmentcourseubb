/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TextInput,
    Linking,
    Navigator
} from 'react-native';

import PlayerList from './PlayerList';
import EditPlayer from './EditPlayer';
import Root from './root';

export default class AwesomeProject extends Component {

    renderScene(route, navigator) {
        if(route.name == 'root') {
            return <Root navigator={navigator} />
        }
        if(route.name == 'playerlist') {
            return <PlayerList navigator={navigator} />
        }
        if(route.name == 'editPlayer') {
            return <EditPlayer navigator={navigator} player={route.data} playerIndex={route.playerIndex} callback={route.callback}/>
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Navigator
                    initialRoute={{name: 'root'}}
                    renderScene={this.renderScene.bind(this)}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
});

AppRegistry.registerComponent('AwesomeProject', () => AwesomeProject);