package com.example.alex.mobiledevelopmentcourselab1;

import java.util.Date;

/**
 * Created by Alex on 11/7/2016.
 */

public class Player {
    private int id;
    private String name;
    private String description;
    private Date birthDate;
    private int[] performance;

    public Player(int id, String name, String description, Date birthDate, int[] performance) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.birthDate = birthDate;
        this.performance = performance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public int[] getPerformance() {
        return performance;
    }

    public void setPerformance(int[] performance) {
        this.performance = performance;
    }

}
