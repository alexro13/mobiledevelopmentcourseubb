package com.example.alex.mobiledevelopmentcourselab1;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.Cursor;
import android.content.Context;
import android.content.ContentValues;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DBWrapper extends SQLiteOpenHelper{
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "FootballPlayers.db";
    //PLAYERS TABLE
    private static final String TABLE_PLAYERS = "players";
    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_DESCRIPTION = "description";
    private static final String COLUMN_BIRTHDATE = "birthdate";
    private static final String COLUMN_PERFORMANCE = "performance";
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);

    public DBWrapper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DB_NAME, factory, DB_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String steps_query = "CREATE TABLE " + TABLE_PLAYERS + " ( "+
                COLUMN_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, " +
                COLUMN_NAME + " INTEGER, " +
                COLUMN_DESCRIPTION + " TEXT, " +
                COLUMN_BIRTHDATE + " DATETIME DEFAULT CURRENT_TIMESTAMP, " +
                COLUMN_PERFORMANCE + " TEXT" +
                " );";
        db.execSQL(steps_query);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLAYERS);
        onCreate(db);
    }

    public static String implode(String separator, int... foo) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0 ; i < foo.length ; i++) {
            sb.append(foo[i]);
            if (i != foo.length - 1) {
                sb.append(separator);
            }
        }
        return sb.toString();
    }

    boolean savePlayer(Player player){
        if (player != null)
        {
            ContentValues values = new ContentValues();
            values.put(COLUMN_ID, player.getId());
            values.put(COLUMN_NAME, player.getName());
            values.put(COLUMN_DESCRIPTION, player.getDescription());
            values.put(COLUMN_BIRTHDATE, dateFormat.format(player.getBirthDate()));
            values.put(COLUMN_PERFORMANCE, implode(" ", player.getPerformance()));
            SQLiteDatabase db = getWritableDatabase();
            db.execSQL("DELETE FROM " + TABLE_PLAYERS + " WHERE " + COLUMN_ID + " ='" + player.getId() + "';");
            db.insert(TABLE_PLAYERS, null, values);
            
            return true;
        }
        return false;
    }

    public Player getPlayer(int id){
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + TABLE_PLAYERS + " WHERE " + COLUMN_ID + "='" + id + "';", null);
        if (c.moveToFirst())
        {
            try {
                String[] parts = c.getString(4).split(" ");
                int[] performance = new int[parts.length];
                for(int n = 0; n < parts.length; n++) {
                    performance[n] = Integer.parseInt(parts[n]);
                }
                Player p = new Player(
                        Integer.parseInt(c.getString(0)),
                        c.getString(1),
                        c.getString(2),
                        dateFormat.parse(c.getString(3)),
                        performance
                );
                c.close();
                
                return p;
            } catch (Exception ignored) {
                c.close();
                
                return null;
            }
        }
        c.close();
        

        return null;
    }

    private int getPlayerCount() {
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_PLAYERS;
        Cursor c = db.rawQuery(sql, null);
        int count = c.getCount();
        c.close();
        return count;
    }

    Player[] getPlayers(){
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + TABLE_PLAYERS + ";", null);
        int playerCount = this.getPlayerCount();
        Player[] players = new Player[playerCount];
        if (c.moveToFirst())
        {
            for (int i = 0 ; i < playerCount ; i++) {
                try {
                    String[] parts = c.getString(4).split(" ");
                    int[] performance = new int[parts.length];
                    for(int n = 0; n < parts.length; n++) {
                        performance[n] = Integer.parseInt(parts[n]);
                    }
                    players[i] = new Player(
                            Integer.parseInt(c.getString(0)),
                            c.getString(1),
                            c.getString(2),
                            dateFormat.parse(c.getString(3)),
                            performance
                    );
                } catch (Exception e) {e.printStackTrace();}
                c.moveToNext();
            }
        }
        c.close();
        
        return players;
    }

    public void deletePlayer(int id) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_PLAYERS + " WHERE " + COLUMN_ID + " ='" + id + "';");
    }

}