package com.example.alex.mobiledevelopmentcourselab1;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by Alex on 11/7/2016.
 */

public class SecondaryFragment extends Fragment {
    View view;
    private ListView mPlaceListView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.secondary_layout, container, false);
        } catch (InflateException ignored) {}

        final TextView name = (TextView) view.findViewById(R.id.player_name_field);
        final TextView description = (TextView) view.findViewById(R.id.player_description_field);


        final DBWrapper dbWrapper = new DBWrapper(getContext(), null, null, 1);
        Player[] players = dbWrapper.getPlayers();
        final ArrayList<Player> mPlayerList = new ArrayList<>();
        for (Player p : players) {
            mPlayerList.add(p);
        }
        mPlaceListView = (ListView) view.findViewById(R.id.player_list_view);
        PlayerListAdapter adapter = new PlayerListAdapter(getActivity(), mPlayerList);
        mPlaceListView.setAdapter(adapter);
        mPlaceListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent appInfo = new Intent(getActivity(), EditPlayerActivity.class);
                appInfo.putExtra("PLAYER_ID", ""+mPlayerList.get(position).getId());
                startActivity(appInfo);
            }
        });

        Button addPlayerBtn = (Button) view.findViewById(R.id.addPlayerBtn);
        addPlayerBtn.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent appInfo = new Intent(getActivity(), EditPlayerActivity.class);
                startActivity(appInfo);
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        DBWrapper dbWrapper = new DBWrapper(getContext(), null, null, 1);
        Player[] players = dbWrapper.getPlayers();
        final ArrayList<Player> mPlayerList = new ArrayList<>();
        for (Player p : players) {
            mPlayerList.add(p);
        }
        mPlaceListView = (ListView) view.findViewById(R.id.player_list_view);
        PlayerListAdapter adapter = new PlayerListAdapter(getActivity(), mPlayerList);
        mPlaceListView.setAdapter(adapter);
        mPlaceListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent appInfo = new Intent(getActivity(), EditPlayerActivity.class);
                appInfo.putExtra("PLAYER_ID", "" + mPlayerList.get(position).getId());
                startActivity(appInfo);
            }
        });
    }

}
