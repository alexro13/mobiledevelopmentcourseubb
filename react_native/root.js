'use strict';
import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    AsyncStorage,
    Text,
    View,
    Linking,
    TextInput,
    BackAndroid
} from 'react-native';

import Button from 'react-native-button';

class Root extends Component {

    constructor() {
        super();
        this.state = {
            text: ''
        }
    }

    componentDidMount(){
        BackAndroid.addEventListener('hardwareBackPress', () => {
            if (this.props.navigator && this.props.navigator.getCurrentRoutes().length > 1) {
            this.props.navigator.pop();
            return true;
        }
        return false;
    });
    }

    handleButtonPress() {
        Linking.openURL('mailto:' + this.state.text);
    }

    navigate(routeName) {
        this.props.navigator.push({
            name: routeName
        });
    }

    render() {
        return (
            <View style={styles.container} refreshing>

                <TextInput
                    style={{width: 275, height: 50, borderColor: 'white', borderWidth: 1}}
                    onChangeText={(text) => this.setState({text})}
                    value={this.state.text}
                    underlineColorAndroid="red"
                    placeholder={"E-mail address"}
                />
                <Button
                    style={styles.secondButton}
                    styleDisabled={{color: 'red'}}
                    onPress={() => this.handleButtonPress()}
                    >
                    <Text style={styles.secondButtonText}>SEND AN EMAIL TO THIS ADDRESS</Text>
                </Button>

                <TouchableHighlight onPress={ this.navigate.bind(this, 'playerlist') } style={styles.button}>
                    <Text style={styles.buttonText}>Player List</Text>
                </TouchableHighlight>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        padding: 10,
        paddingTop: 150,
    },
    button: {
        height: 75,
        backgroundColor: '#E0E0E0',
        alignSelf: 'stretch',
        alignItems: 'center',
        marginTop: 225,
        justifyContent: 'center'
    },
    secondButton: {
        textAlign: 'center',
        width: 275,
        height: 50,
        fontSize: 15,
        color: 'black',
        backgroundColor:'#E0E0E0',
    },
    secondButtonText: {
        alignSelf: 'center',
        textAlign: 'center',
        marginVertical: 10,
        width: 275,
        height: 50
    },
    buttonText: {
        fontSize: 22,
        color: '#000000',
        alignSelf: 'center'
    },
    title: {
        fontSize: 25,
        marginBottom: 15
    }
});


export default Root