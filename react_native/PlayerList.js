'use strict';
import React, { Component } from 'react';
import {
    StyleSheet,
    TextInput,
    TouchableHighlight,
    AsyncStorage,
    Text,
    View
} from 'react-native';

import Button from 'react-native-button';

class PlayerList extends Component {
    constructor(){
        super();
        this.edit = this.edit.bind(this);
        this.state = {
            players: [],
            newPlayer: ''
        }
    }

    delete(index) {
        var playerlist = this.state.players;
        var playerlistAfterDelete = [];
        var i = 0;
        playerlist.map((player) => {
            if (index != i) {
                playerlistAfterDelete.push(player);
            }
            i += 1;
        });
        delete playerlist[index];
        this.setState({players: playerlist});
        try {
            AsyncStorage.setItem('@playerList:key', JSON.stringify(playerlistAfterDelete)).then((data) => {
        });
        } catch (error) {}
    }

    savePlayer() {
        var playerlist = this.state.players;
        var newPlayer = this.state.newPlayer.split(" ");
        playerlist.push({name: newPlayer[0], description: newPlayer[1]});
        this.setState({players: playerlist});
        try {
            AsyncStorage.setItem('@playerList:key', JSON.stringify(playerlist)).then((data) => {
        });
        } catch (error) {}
        this.setState({newPlayer: ''})
    }

    navigate(routeName, data, index) {
        this.props.navigator.push({
            name: routeName,
            data: data,
            callback: this.edit,
            playerIndex: index
        });
    }

    edit(playerName, playerDescription, index) {
        var playerlist = this.state.players;
        playerlist[index].name = playerName;
        playerlist[index].description = playerDescription;
        this.setState({players: playerlist});
        try {
            AsyncStorage.setItem('@playerList:key', JSON.stringify(playerlist)).then((data) => {});
        } catch (error) {}
    }

    componentWillMount() {
        try {
            AsyncStorage.getItem('@playerList:key').then((data) => {
                if (data) {
                    this.setState({players: JSON.parse(data)});
                }
            });
        } catch (error) {}
    }

    render() {

        const playerList = this.state.players.map((data, index) => {
            return (
                <View key={index} refreshing>
                    <Button style={styles.playerStyle} onPress={ this.navigate.bind(this, 'editPlayer', data, index) }>
                        <Text style={styles.playerText}>{data.name + ' ' + data.description}</Text>
                    </Button>
                    <Button onPress={ this.delete.bind(this, index) }>
                        <Text style={styles.playerText}>Delete</Text>
                    </Button>
                </View>
            )
        });

        return (
            <View style={styles.container} refreshing>
                <TextInput
                    style={{width: 150,height: 40, borderColor: 'gray', borderWidth: 1}}
                    onChangeText={(newPlayer) => this.setState({newPlayer})}
                    value={this.state.newPlayer}
                />

                <Button onPress={ this.savePlayer.bind(this) }>Save</Button>

                <Text style={styles.heading}>
                    Player list:
                </Text>
                {playerList}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        padding: 10,
        paddingTop: 80
    },
    input: {
        height: 50,
        marginTop: 10,
        padding: 4,
        fontSize: 18,
        borderWidth: 1,
        borderColor: '#48bbec'
    },
    button: {
        height: 50,
        backgroundColor: '#48BBEC',
        alignSelf: 'stretch',
        marginTop: 10,
        justifyContent: 'center'
    },
    playerStyle: {
        height: 75,
        backgroundColor: '#FFAA00',
        alignSelf: 'stretch',
        marginTop: 10,
        justifyContent: 'center',
    },
    buttonText: {
        fontSize: 22,
        color: '#FFF',
        alignSelf: 'center'
    },
    heading: {
        fontSize: 30,
    },
    error: {
        color: 'red',
        paddingTop: 10
    },
    success: {
        color: 'green',
        paddingTop: 10
    },
    loader: {
        marginTop: 20
    },
    playerText: {
        height: 50,
        backgroundColor: '#9E9E9E',
        alignSelf: 'stretch',
        marginTop: 25,
        justifyContent: 'center',
        color: '#FFFFFF'
    }
});

export default PlayerList