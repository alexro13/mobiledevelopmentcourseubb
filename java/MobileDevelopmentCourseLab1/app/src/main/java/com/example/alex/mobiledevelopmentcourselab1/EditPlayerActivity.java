package com.example.alex.mobiledevelopmentcourselab1;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by Alex on 11/7/2016.
 */

public class EditPlayerActivity extends Activity {
    DatePickerDialog dateStartPicker;
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);
    DBWrapper dbWrapper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        dbWrapper = new DBWrapper(getApplicationContext(), null, null, 1);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.player_edit_activity);
        final TextView playerIDField = (TextView) findViewById(R.id.player_id_field);
        final TextView playerNameField = (TextView) findViewById(R.id.player_name_field);
        final TextView playerDescriptionField = (TextView) findViewById(R.id.player_description_field);
        final TextView playerBirthDate = (TextView) findViewById(R.id.playerBirthDateTextField);
        final TextView playerPerformanceField = (TextView) findViewById(R.id.performance_text);
        playerBirthDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                dateStartPicker = new DatePickerDialog(EditPlayerActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        Calendar c = Calendar.getInstance();
                        c.set(i, i1, i2);
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);
                        playerBirthDate.setText(df.format(c.getTime()));
                    }
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                Calendar start = (Calendar) c.clone();
                start.set(Calendar.YEAR, 1900);
                dateStartPicker.getDatePicker().setMinDate(start.getTimeInMillis());
                dateStartPicker.show();
            }
        });

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey("PLAYER_ID")) {
            Button deletePlayerBtn = (Button) findViewById(R.id.player_delete_btn);
            final Player player = dbWrapper.getPlayer(Integer.parseInt(extras.getString("PLAYER_ID")));
            deletePlayerBtn.setOnClickListener(new AdapterView.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dbWrapper.deletePlayer(player.getId());
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(getApplicationContext(), "Deleted succesfully", duration);
                    toast.show();
                    finish();
                }
            });
            playerIDField.setText(String.valueOf(player.getId()));
            playerNameField.setText(player.getName());
            playerDescriptionField.setText(player.getDescription());
            playerBirthDate.setText(df.format(player.getBirthDate()));
            playerPerformanceField.setText(DBWrapper.implode(" ", player.getPerformance()));
            LineChart chart = (LineChart) findViewById(R.id.chart);
            int[] performance = player.getPerformance();

            List<Entry> entries = new ArrayList<Entry>();
            int i = 0;
            for (int data : performance) {
                // turn your data into Entry objects
                entries.add(new Entry(i, data));
                i++;
            }
            LineDataSet dataSet = new LineDataSet(entries, "Performance");
            LineData lineData = new LineData(dataSet);
            chart.setData(lineData);
            chart.invalidate(); // refresh
        }
        Button saveBtn = (Button) findViewById(R.id.playerSave);
        saveBtn.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                DBWrapper dbWrapper = new DBWrapper(getApplicationContext(), null, null, 1);
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);
                try{
                    String[] parts = playerPerformanceField.getText().toString().split(" ");
                    int[] performance = new int[parts.length];
                    for(int n = 0; n < parts.length; n++) {
                        performance[n] = Integer.parseInt(parts[n]);
                    }
                    Player p = new Player(
                            Integer.parseInt(playerIDField.getText().toString()),
                            playerNameField.getText().toString(),
                            playerDescriptionField.getText().toString(),
                            df.parse(playerBirthDate.getText().toString()),
                            performance
                    );
                    int duration = Toast.LENGTH_SHORT;
                    String text = dbWrapper.savePlayer(p) ? "Saved succesfully" : "An error has occured";
                    Toast toast = Toast.makeText(getApplicationContext(), text, duration);
                    toast.show();
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
