package com.example.alex.mobiledevelopmentcourselab1;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by Alex on 11/7/2016.
 */

public class MainFragment extends Fragment {
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.main_layout, container, false);
        } catch (InflateException ignored) {}
        final TextView email = (TextView) view.findViewById(R.id.editText2);
        Button fab = (Button) view.findViewById(R.id.button);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setType("plain/text");
                sendIntent.setData(Uri.parse("test@gmail.com"));
                sendIntent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
                sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[] { email.getText().toString() });
                sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Test mail");
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Testing testing 1 2 3. Is this thing on?");
                startActivity(sendIntent);
            }
        });
        return view;
    }
}
