Footbal Team Coach
Add players via an input form.
Edit players via a select list / input form.
Send mails to the players to let them know if they have made the team or not.
Display a chart based on the player's past performance.
The players data will be saved in local storage.
The players data will sync with a remote server when internet connection is available.
