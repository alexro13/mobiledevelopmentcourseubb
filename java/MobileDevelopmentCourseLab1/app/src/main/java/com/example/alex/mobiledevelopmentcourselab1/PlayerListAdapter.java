package com.example.alex.mobiledevelopmentcourselab1;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Alex on 11/7/2016.
 */

public class PlayerListAdapter extends BaseAdapter {
    private final Context context;
    private ArrayList<Player> mPlayers;

    public PlayerListAdapter(Context context, ArrayList<Player> places) {
        this.context = context;
        this.mPlayers = places;
    }

    @Override
    public int getCount() {
        return mPlayers.size();
    }

    @Override
    public Object getItem(int i) {
        return mPlayers.get(i);
    }

    @Override
    public long getItemId(int i) {
        return mPlayers.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            view = mInflater.inflate(R.layout.player_list_item, null);
        }
        TextView playerName = (TextView)view.findViewById(R.id.player_name);
        playerName.setText(mPlayers.get(i).getName());

        return view;
    }
}
