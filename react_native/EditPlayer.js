'use strict';
import React, { Component } from 'react';
import {
    StyleSheet,
    TextInput,
    TouchableHighlight,
    AsyncStorage,
    Text,
    View,
    BackAndroid
} from 'react-native';

import Button from 'react-native-button';
import { Pie } from 'react-native-pathjs-charts'

class EditPlayer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            playerName: props.player.name,
            playerDescription: props.player.description
        }
    }

    navigate(routeName) {
        this.props.navigator.push({
            name: routeName
        });
    }

    edit() {
        this.props.callback(this.state.playerName, this.state.playerDescription, this.props.playerIndex);
        this.props.navigator.pop();
    }
    render() {
        var sampleData = {
            pie: {
                data: [{
                    "name": "Win",
                    "population": 42376524
                }, {
                    "name": "Lose",
                    "population": 100000000 - 42376524
                }],
                options: {
                    margin: {
                        top: 0,
                        left: 0,
                        right: 0,
                        bottom: 0
                    },
                    width: 300,
                    height: 300,
                    color: '#2980B9',
                    r: 75,
                    R: 150,
                    legendPosition: 'topLeft',
                    animate: {
                        type: 'oneByOne',
                        duration: 200,
                        fillTransition: 3
                    },
                    label: {
                        fontFamily: 'Arial',
                        fontSize: 14,
                        fontWeight: true,
                        color: '#ECF0F1'
                    }
                }
            }
        };
        return (
            <View style={styles.container} refreshing>
                <TextInput
                    style={{width: 150,height: 40}}
                    onChangeText={(text) => this.setState({playerName: text})}
                    value={this.state.playerName}
                />
                <TextInput
                    style={{width: 150,height: 40, marginTop: 100}}
                    onChangeText={(text) => this.setState({playerDescription: text})}
                    value={this.state.playerDescription}
                />
                <Button onPress={ this.edit.bind(this) }>Save</Button>

                <Pie
                    data={sampleData.pie.data}
                    options={sampleData.pie.options}
                    accessorKey="population" />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        padding: 10,
        paddingTop: 80
    },
    input: {
        height: 50,
        marginTop: 10,
        padding: 4,
        fontSize: 18,
        borderWidth: 1,
        borderColor: '#48bbec'
    },
    button: {
        height: 50,
        backgroundColor: '#48BBEC',
        alignSelf: 'stretch',
        marginTop: 10,
        justifyContent: 'center'
    },
    buttonText: {
        fontSize: 22,
        color: '#FFF',
        alignSelf: 'center'
    },
    heading: {
        fontSize: 30,
    },
    error: {
        color: 'red',
        paddingTop: 10
    },
    success: {
        color: 'green',
        paddingTop: 10
    },
    loader: {
        marginTop: 20
    }
});

export default EditPlayer